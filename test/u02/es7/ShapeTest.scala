package u02.es7

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.{Test}

class ShapeTest {

  import GeometricShape.GeomShape._
  val circle  = Circle(7.2)
  val rectangle = Rectangle(2,4)
  val square = Square(5)
  @Test
  def testPerimeter(){
    assertEquals(perimeter(circle),45.23893421169302)
    assertEquals(perimeter(rectangle),12)
    assertEquals(perimeter(square),20)
  }
  @Test
  def testAreas(){
    assertEquals(area(circle),162.8601631620949)
    assertEquals(area(rectangle),8)
    assertEquals(area(square),25)
  }
}
