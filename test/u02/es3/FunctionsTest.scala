package u02.es3

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.{Test}

class FunctionsTest {
  import Functions._
  @Test
  def testParity() {
    assertEquals(parity(5), "Odd")
    assertEquals(parity(22), "Even")
  }

  @Test
  def testNeg(): Unit ={
    val empty : Int => Boolean = _==""
    val emptyString :String => Boolean = _==""

    val notEmpty = neg(empty)
    val notEmptyString = neg(emptyString)

    assertEquals(notEmpty(2),true)
    assertEquals(notEmptyString(""),false)
    assertEquals(notEmptyString("foo")&& !notEmptyString(""),true)

  }
}
