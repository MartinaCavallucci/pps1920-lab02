package u02.es6

object Fibonacci extends App{

  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fib(n-1) + fib(n-2)
  }

  println((fib(0),fib(1),fib(2),fib(3),fib(4)))// (0,1,1,2,3)

  /*
    Fibonacci with tail recursion
   */

  def fib2 ( n : Int ) : Int = {
    @annotation.tailrec // checks only if optimisation is possible
    def _fib ( n : Int , base : Int , res:Int) : Int = n match {
      case 0 => base
      case 1 => res
      case _=> _fib(n-1,res,base+res)
    }
    _fib (n , 0,1)
  }

  // _fib(3,0,1) ->_fib(2,1,1)->_fib(1,1,2)->_fib(0,2,3)->return 2 in case 0 => base
  println((fib2(0),fib2(1),fib2(2),fib2(3),fib2(4)))// (0,1,1,2,3)
}
