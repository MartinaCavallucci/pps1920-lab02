package u02.es3

object Functions extends App {
  /*
  Esercizio 3a [funzione parity]
   */
  def parity(x:Int):String = x%2 match {
    case 0 => "Even"
    case 1 => "Odd"
  }
  /*
    Esercizio 3b [ Funzione neg generica]
   */
  def neg[A](pred: A=>Boolean): (A=>Boolean) =  !pred(_)

}
