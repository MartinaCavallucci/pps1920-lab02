package u02.es7



object GeometricShape extends App{
  sealed trait GeomShape
    object GeomShape{
      case class Rectangle(basis: Double, height: Double) extends GeomShape
      case class Circle(radius: Double) extends GeomShape
      case class Square(side: Double) extends GeomShape

      def perimeter(shape:GeomShape):Double = shape match {
        case Rectangle(b, h) => 2*(b+h)
        case Circle(r) => 2*r*Math.PI
        case Square(s) => 4*s
      }
      def area(shape:GeomShape):Double = shape match {
        case Rectangle(b, h) => b*h
        case Circle(r) => Math.PI*Math.pow(r,2)
        case Square(s) => Math.pow(s,2)
      }

    }

}
