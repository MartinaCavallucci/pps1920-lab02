package u02.es4

object Curring extends App {
  //Esercizio 4
  /*
    val CurriedFunType
   */
  val p1: Int => Int => Int => Boolean = x=>y=>z=> (x <= y )  && (y <= z)
  /*
    val NonCurriedFunType
   */
  val p2:(Int,Int,Int)=>Boolean = (x,y,z)=> (x <= y )  && (y <= z)
  /*
    Function with  currying
    i.e.  def CurriedFunType
  */
  def p3(x:Int)(y:Int)(z:Int):Boolean = (x <= y )  && (y <= z)

  /*
     Standard function with no currying
     i.e.  def NonCurriedFunType
   */
  def p4(x:Int,y:Int,z:Int):Boolean = (x <= y )  && (y <= z)

  println(p1(1)(2)(3)) // true
  println(p2(1,2,3)) //true
  println(p3(1)(2)(3)) //true
  println(p4(1,2,3)) //true
}
