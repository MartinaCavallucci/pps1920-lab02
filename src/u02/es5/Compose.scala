package u02.es5

object Compose extends App {

  def compose[A](f: A=>A , g: A=>A):A => A = i => f(g(i))
  println(compose[Int](_-1,_*2)(5) )// 9)
  println(compose[Double](_-1,_*2)(5.2) )// 9)
}
